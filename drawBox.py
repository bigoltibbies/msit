import pyperclip
import pyautogui
import time
import random
import subprocess
import mod
from pynput.keyboard import Key, Listener

class rectangle:
    def __init__(self, x, y, r=None, g=None, b=None, w=None, h=None):
        self.x = x
        self.y = y
        self.r = r or []
        self.g = g or []
        self.b = b or []
        self.w = w or []
        self.h = h or []


rec = rectangle(0,0,0,0,0,0,0)

def topLeft():
	global rec
	x, y = pyautogui.position()
	rec.x = x
	rec.y = y
	txt = f"rec = rectangle(x={rec.x}, y={rec.y})"
	pyperclip.copy(txt)
	print(txt)
	
def bottomRight():
	global rec
	x, y = pyautogui.position()
	rec.w = x - rec.x
	rec.h = y - rec.y
	txt = f"rec = rectangle(x={rec.x}, y={rec.y}, w={rec.w}, h={rec.h})"
	pyperclip.copy(txt)
	print(txt)
	
def color():
	global rec
	x, y = pyautogui.position()
	r,g,b = pyautogui.pixel(x, y)
	rec.r = r
	rec.g = g
	rec.b = b
	txt = f"rec = rectangle(x={rec.x}, y={rec.y}, w={rec.w}, h={rec.h}, r={rec.r}, g={rec.g}, b={rec.b})"
	pyperclip.copy(txt)
	print(txt)

def makeRec():
	attacking= recRGB(1392, 68, 254, 0, 0, w=30, h=100)
	x, y = pyautogui.position()
	r,g,b = pyautogui.pixel(x, y)
	print(f"RGB: {r}, {g}, {b}")

def on_press(key):
	print('{0} pressed'.format(
		key))

def on_release(key):
	print('{0} release'.format(
		key))
	if key == Key.esc:
		# Stop listener
		return False
	if 'char' in dir(key):  
		if key.char == 'a': 
			topLeft()
		if key.char == 's': 
			bottomRight()
		if key.char == 'd':
			color()


# Collect events until released
with Listener(
		on_press=on_press,
		on_release=on_release) as listener:
	listener.join()
