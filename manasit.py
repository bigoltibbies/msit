from pyautogui import *
import cv2 as cv
import numpy as np
import pyautogui
import asyncio
import time
import random
import concurrent.futures
import threading
import subprocess
import keyboard
from matplotlib import pyplot as plt
from threading import Thread


class rectangle:
    def __init__(self, x, y, w=0, h=0, r=0, g=0, b=0):
        self.x = x
        self.y = y
        self.w = w or 0
        self.h = h or 0
        self.r = r or 0
        self.g = g or 0
        self.b = b or 0

charPosition = rectangle(x=497, y=324)
#mana = rectangle(x=1081, y=12, w=33, h=2, r=42, g=42, b=42)
mana = rectangle(x=780, y=35, w=39, h=3, r=42, g=42, b=42)
#manaPotion = rectangle(x=1022, y=183, w=25, h=19, r=158, g=2, b=156)
manaPotion = rectangle(x=1015, y=193, w=35, h=36, r=187, g=74, b=217)
#haveROH = rectangle(x=1193, y=236, w=26, h=25, r=249, g=124, b=99)
haveROH = rectangle(x=128, y=588, w=29, h=29, r=253, g=188, b=171)
vial = rectangle(x=1745, y=461, w=42, h=39, r=250, g=179, b=91) 
#wearingROH = rectangle(x=1193, y=236, w=26, h=25, r=206, g=81, b=68)
wearingROH = rectangle(x=1193, y=235, w=26, h=28, r=253, g=188, b=171)
#wearingLifeRing 
#haveLifeRing

# search for manas, drag 100
# search for rings, retrieve all
# stow empty vials

def findPixel(r):
    pic = pyautogui.screenshot(region=(r.x, r.y, r.w, r.h))
    width, height = pic.size
    xpos=r.x
    ypos=r.y
    red = r.r
    green = r.g
    blue = r.b
    for x in range(0, width, 1):
        for y in range (0, height, 1):
            r,g,b = pic.getpixel((x, y))
            if r == red and g == green and b == blue:
                print(f"x and y: {x}, {y}")
                return True
    return False



def findImageMap(img, c=0.6):
    global map
    img_rgb = pyautogui.screenshot(region=(map.x, map.y, map.w, map.h))
    img_rgb.save("ss.png")
    img_rgb = cv.imread('ss.png')
    assert img_rgb is not None, "file could not be read, check with os.path.exists()"
    img_gray = cv.cvtColor(img_rgb, cv.COLOR_BGR2GRAY)
    template = cv.imread(f"{img}.png", cv.IMREAD_GRAYSCALE)
    assert template is not None, "file could not be read, check with os.path.exists()"
    w, h = template.shape[::-1]
    res = cv.matchTemplate(img_gray,template,cv.TM_CCOEFF_NORMED)
    threshold = c
    loc = np.where( res >= threshold)
    min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
    for pt in zip(*loc[::-1]):
        cv.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
        offset = 4
        xpos = max_loc[0] + map.x + offset
        ypos = max_loc[1] + map.y + offset
        print(f"found {img}: {xpos},{ypos}")
        cv.imwrite('res.png', img_rgb)
        return (xpos, ypos)
    print(f"no image {img}")
    return None

def findImage(img, c=0.5):
    name = f"{img}.png"
    res = pyautogui.locateCenterOnScreen(name, confidence = 0.5)
    if res != None:
        print(f"found image {img}")
        return res
    else:
        print(f"no image {img}")
        time.sleep(0.5)
        return None

def gotoImageMap(name, click=None, c=0.6):
    pos = pyautogui.position()
    res = findImageMap(name, c)
    if res != None:
        pyautogui.moveTo(res)
        if click != None:
            if click == "R":
                pyautogui.rightClick()
            if click == "L":
                pyautogui.leftClick()
        pyautogui.moveTo(pos)
        return True
    else:
        return False


def gotoImage(name, click=None, c=0.5):
    pos = pyautogui.position()
    res = findImage(name, c)
    if res != None:
        pyautogui.moveTo(res)
        if click != None:
            if click == "R":
                pyautogui.rightClick()
            if click == "L":
                pyautogui.leftClick()
        pyautogui.moveTo(pos)
        return True
    else:
        return False


noMarkersThreshold=0
def walkTo(name, button, seconds=10):
    global noMarkersThreshold 
    checkLoot()
    if gotoImageMap(name, button):
        noMarkersThreshold=0
    else:
        noMarkersThreshold+=1
        if noMarkersThreshold >= 3:
            xlog("no markers.png")

    for x in range(seconds):
        if findMonster():
            while findMonster():
                checkLoot()
                attack()
                sleep(1)
                checkLoot()
            gotoImageMap(name, button)
        sleep(1)

def mapPadlock(seconds):
    walkTo("padlock", "L", seconds)

def mapSword(seconds):
    walkTo("sword", "L", seconds)

def mapSkull(seconds):
    walkTo("skull", "L", seconds)

def mapQuestion(seconds):
    walkTo("question", "L", seconds)

def mapDownRed(seconds):
    walkTo("downRed", "L", seconds)

def mapUpRed(seconds):
    walkTo("upRed", "L", seconds)

def mapBag(seconds):
    walkTo("bag", "L", seconds)

def mapTick(seconds):
    walkTo("tick", "L", seconds)

def mapStar(seconds):
    walkTo("star", "L", seconds)

def mapUpGreen(seconds):
    walkTo("upGreen", "L", seconds)

def mapExclamation(seconds):
    walkTo("exclamation", "L", seconds)

def mapUp(seconds):
    walkTo("up", "L", seconds)

def attackMonster():
    gotoImage("wasp", "L")

def clearLootWindow():
    gotoImage("lootWindow", "R")
    gotoImage("clearWindow", "L")

def checkLoot():
    global loot
    if findPixel(loot):
        print("loot dropped")
        lootCreature()
    else:
        print("no loot")

def lootCreature():
    subprocess.call("python3 clicky.py 1", shell=True)
    time.sleep(1)
    clearLootWindow()

def sendKey(key):
    pyautogui.keyDown(key)
    pyautogui.keyUp(key)
    #time.sleep(1)

def sendControlClick():
    pyautogui.keyDown('ctrl')
    pyautogui.click()
    pyautogui.keyUp('ctrl')

attackAnything=0
def attackAnythingThreshold():
    global attackAnything
    print(f"attack anything? {attackAnything}")
    if attackAnything >= 10:
        sendKey('space')
        attackAnything=0
    else:
        attackAnything+=1
        print(f"attack anything? {attackAnything}")

def rope():
    global charPosition
    pyautogui.moveTo(charPosition.x, charPosition.y)
    sendKey('F7')
    sleep(0.5)
    pyautogui.leftClick()
    sleep(2)

def xlog(name):
    print(name)
    sendKey('F6')
    sendKey('F6')
    sendKey('F1')
    sendKey('F6')
    sendKey('F6')
    img_rgb = pyautogui.screenshot()
    img_rgb.save(name)
    subprocess.call("pkill -fi 'Cipsoft'", shell=True)
    subprocess.call("pkill -fi 'gloom.py'", shell=True)
    subprocess.call("pkill -fi 'manasit.py'", shell=True)


def ringTask():
    p = 1
    while p < 6:
        ring()
        sleep(2)

def useSpellTask():
    p = 1
    while p < 6:
        useSpell()
        sleep(1)

def foodTask():
    p = 1
    while p < 6:
        sendKey('F5')
        sleep(120)

def ring():
    global health
    global mana
    global haveROH
    global wearingROH
    if findPixel(haveROH):
        if not findPixel(wearingROH):
            sendKey('F4')
            sendKey('F5')
            sendKey('F5')
            sendKey('F5')
            sendKey('F5')
            print("equipped ROH")

def getManas():
    pos = pyautogui.position()
    res = findImage("locker")
    if res != None:
        pyautogui.moveTo(res)
        gotoImage("search", "L")
        pyautogui.typewrite("mana potion")
        gotoImage("withdrawMana", "L")
        gotoImage("eye", "L")
        gotoImage("retrieve", "L")
        gotoImage("x", "L")
#        sendKey('m')


def useSpell():
    if not findPixel(mana):
    #    print("using spells")
        sendKey('F3')

def useMana():
    global charPosition
    global manaPotion 
    pyautogui.moveTo(manaPotion.x+15, manaPotion.y+15)
    pyautogui.rightClick()
#    gotoImage("mana", "R")
    pyautogui.moveTo(charPosition.x, charPosition.y)
    pyautogui.leftClick()
#    if not findPixel(manaPotion):
#        xlog("no manas.png")

def stow():
    global vial
    pos = pyautogui.position()
    res = findImage("vial")
    if res != None:
        pyautogui.moveTo(res)
        sleep(1)
        sendolClick()
        sleep(1)
        gotoImage("stow", "L")

#t1 = Thread(target=ringTask)
#t1.start()

#t2 = Thread(target=useSpellTask)
#t2.start()
#t3 = Thread(target=foodTask)
#t3.start()

def main():
    t1 = Thread(target=useMana)
    t2 = Thread(target=useSpell)
    t3 = Thread(target=ring)
    t1.start()
    t2.start()
    t3.start()

    t1.join()
    t2.join()
    t3.join()

#async def call_tests():
#    await asyncio.gather(useMana(), useSpell(), ring())

i=1
while i < 6:
    main()
#    asyncio.run(main())
    #asyncio.run(main())
#    useMana()
#    useSpell()
    #sleep(0.2)
